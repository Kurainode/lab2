package lab2;

import org.uncommons.watchmaker.framework.operators.AbstractCrossover;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MyCrossover extends AbstractCrossover<double[]> {
    protected MyCrossover() {
        super(1);
    }

    protected List<double[]> mate(double[] p1, double[] p2, int i, Random random) {
        ArrayList<double[]> children = new ArrayList<double[]>();

        // your implementation:

        double[] p1bis = p1.clone();
        double[] p2bis = p2.clone();

        for (int j = random.nextInt(p1.length - 1) + 1; j < p1.length; ++j) {
            p1bis[j] = p2[j];
            p2bis[j] = p1[j];
        }
        children.add(p1bis);
        children.add(p2bis);

        return children;
    }
}
