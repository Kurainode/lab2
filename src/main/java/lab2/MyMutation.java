package lab2;

import org.uncommons.watchmaker.framework.EvolutionaryOperator;

import java.util.List;
import java.util.Random;

public class MyMutation implements EvolutionaryOperator<double[]> {
    public List<double[]> apply(List<double[]> population, Random random) {
        // initial population
        // need to change individuals, but not their number!

        // your implementation:

        double mutationTestProbability = 0.4;
        double mutationApplyProbability = 0.8;

        for (int i = 0; i < population.size(); ++i) {
            if (random.nextDouble() > mutationTestProbability)
                continue;

            double[] toMutate = population.get(i);

            for (int j = 0; j < toMutate.length; ++j) {
                if (random.nextDouble() > mutationApplyProbability)
                    continue;
                double newValue = toMutate[j] * random.nextGaussian();

                // Testing boundaries: if the new value is out of bounds, we need to correct it
                int iter = 0;
                while ( toMutate[j] + newValue > 5.0 || toMutate[j] + newValue < -5.0) {
                    newValue /= 2.0;
                    if (++iter > 10)
                        break;
                }
                if (iter < 10)
                    toMutate[j] += newValue;
            }

            population.set(i, toMutate);
        }

        //result population
        return population;
    }
}
